<?php
#	Application client pour la Base Nationale pstc
# 
# - Inclure ce module dans le serveur pstc
# - Deux fonctions prêtes : pstc_stats ($ndb) et pstc_import ($ndb, $valid)
###

require_once ('./inc_global.php');
require_once("./lib/xmlrpc.inc");

############################################################################# pstc_stats ###
# Demande de statistiques sur la base de données numéro $ndb
# Résultat retourné
# - erreur	: message d'erreur
# - ok		: tableau des résultats

function pstc_stats ($ndb) {
	global $adrserver, $login;

    //creation du client
	$client = new xmlrpc_client($adrserver);

	$message=new xmlrpcmsg('pstc_stats',
		array(new xmlrpcval($ndb),
		new xmlrpcval($login),	
	));
	
	//envoi de la requete
	$response=$client->send($message);

	//Traitement de la réponse
	if ($response->faultCode()) {
    	return "ERROR: ".$response->faultCode()." : ".$response->faultString();    // >>>  ERROR EXIT
    
	} else {
		//decodage de la réponse pour la mettre au format php standard
		$result=xmlrpcval_decode($response->value());
		return $result ;
	}
}

############################################################################# pstc_import ###
# Demande de transfert d'une fiche de la base de données
# La fiche est choisie automatiquement
# - en priorité dans les fiches nouvelles en demande de transfert
# - sinon dans les fiches à rafraîchir.
# $ndb 		: Numéro de base
# $valid	: autorise l'écriture des codes d'état dans la fiche
### Résultat retourné
# - erreur	: message d'erreur
#			-> le code d'état de la fiche est mis à 3 (si $valid vrai)
# - ok		: tableau contenant le contenu de la fiche au format PSTC
#			-> le code d'état de la fiche est mis à 2 (si $valid vrai)
# - vide	: plus de fiche à transmettre

function pstc_import ($ndb, $valid) {
	global $adrserver, $login, $debug;

    //creation du client
	$client = new xmlrpc_client($adrserver);

#------------------------------------------------------- Choix de la fiche ---
	$tabetat = array (5, 0);	// Fiches nouvelles, fiches à rafraîchir
	$idcol = 0;

	// On boucle les différents codes d'état possibles
	foreach ($tabetat as $idetat) {

		$message=new xmlrpcmsg('pstc_list',
			array(new xmlrpcval($ndb),
			new xmlrpcval($login),	
			new xmlrpcval($idetat,"string"),
			));
		//envoi de la requete
		$response=$client->send($message);

		//Traitement de la réponse
		if ($response->faultCode()) {
			return "ERROR: ".$response->faultCode()." : ".$response->faultString();    // >>>  ERROR EXIT
		} else {
			//decodage de la réponse pour la mettre au format php standard
			$result=xmlrpcval_decode($response->value());
			if (isset ($result[0])) {
				$idcol = $result[0];			// trouvé une fiche
				break;
			}
		}
	}

	if ($idcol ==0) return "";	    // Aucune fiche à transmettre >>>  EXIT 
	
#------------------------------------------------------- Appel de la fiche ---
	if ($debug)	$client->setDebug(2);			// debug en cas d'erreur côté server

	$message=new xmlrpcmsg('pstc_trans',
		array(new xmlrpcval($ndb),
		new xmlrpcval($login),	
		new xmlrpcval($idcol,"string"),
	));
	//envoi de la requete
	$response=$client->send($message);

	//Traitement de la réponse
	if ($response->faultCode()) {	
		$ack = FALSE;
    	$result = "ERROR: ".$response->faultCode()." : [idcol=$idcol] ".$response->faultString();    
	} else {
		$ack = TRUE;
		//decodage de la réponse pour la mettre au format php standard
		$result=xmlrpcval_decode($response->value());
	}

#------------------------------------------------------- Validation, si demandée ---
	if ($valid) {
		$message=new xmlrpcmsg('pstc_ack',
			array(new xmlrpcval($ndb),
			new xmlrpcval($login),	
			new xmlrpcval($idcol,"string"),
			new xmlrpcval($ack,"string")
		));

		//envoi de la requete
		$response=$client->send($message);
		//Traitement de la réponse
		if ($response->faultCode()) {
			return "ERROR: ".$response->faultCode()." : ".$response->faultString();	    // >>>  ERROR EXIT
		} 
	}

	return $result ;
}

####################################################################### xmlrpcval_decode ###
/*Fonction de recuperation des donnees recues
 *du serveur en XML-RPC
 *Transforme $val du type xmlrpcval
 *en un (tableau (de tableaux) de) string(s)
 *Recursive */
function xmlrpcval_decode ($val) {
    switch ($val->kindOf()) {
	    case "array":
	        /*on recupere un array en tableau indexe*/
	    	$ret=array();
	        for ($i=0;$i<$val->arraysize();$i++) {
			    $ret[$i]=xmlrpcval_decode($val->arraymem($i));
			}
			break;
	    case "struct":
	        /*un structure devient un tableau associatif*/
	    	$ret=array();
	        $val->structreset();
	        while(list($title,$val2)=$val->structeach()) {
			    $ret[$title]=xmlrpcval_decode($val2);
			}
			break;
	    default:
	        /*un scalaire est transforme en string*/
	        $ret=$val->scalarval();
	}
	return $ret;
}



?>
