## WSPSTC - test du système WebSrvice proposé à PATSTEC 
Ce logiciel permet de tester les fonction WebService prmttant d'extraire de DBAconit les éléents fiches et médias pour assurer une synchronisation base locale -> base nationale.

- Une méthode permet d'obtenir la liste des fiches nouvelles ou modifiées de la base locale, qui nécessitent un transfert vers la base nationale.
- Une méthode permet d'appeler les élémnts à charger fiche par fiche, à la demnde.
- Une méthode informe la base locale que le transfert s'est bien réalisé ou bien est en échec (pour la mise à jour de l'indicateur etatfiche).

## Documentation
La documentation se trouve dans le dossier [Documentation](https://db.aconit.org/documentation/),
Un fichier unique documente DBserver, WSclient et WSpstc :
[DBserveur notice technique](https://db.aconit.org/documentation/Notice_DBserveur_v1.pdf)
