<?php
# Programme de test des fonctions proposées pour PSTC
###

require_once("./inc_client.php");

echo $header;

if (isset($_POST['lancement'])) {
########################################## Traitement des actions ###

	$ndb = ($_POST["ndb"]);
	$valid = isset ($_POST["valid"]);
	
	switch ($_POST["test"])  {

	case "pstc_stats" : 
		$res = pstc_stats ($ndb);
		break;
	case "pstc_import" : 
		$res = pstc_import ($ndb, $valid);
	}

	// Affichage du résultat
	echo "<pre>"; print_r ($res); echo "</pre>";
	
} 
###################################################################### Affichage menu de choix ###
?>

	<body>
		<form method=post action="test.php">
			<h2>Choix du test </h2>
			<ul>
				<li>No base :&nbsp; <input type="text" name="ndb" size="20" value="" /> </li>
				<li><input type='radio' name='test' value='pstc_stats'  checked/> pstc_stats </li>
				<li><input type='radio' name='test' value='pstc_import'/> pstc_import </li>
				<li>Autorisation de validation :&nbsp; <input type="checkbox" name="valid"  /> </li>

			<input  style="margin-left:400px;" type="submit" name="lancement" value="Valider" />
			</ul>
		</form>
<?php

echo <<<EOD
	</body>
</html>
EOD;



################################################################################## debug ###
function debug ($code, $msg, $var="", $mix="") {
// Affiche un message et (éventuellement) le contenu d'une variable 
// si un bit positionné de la  variable $debug correspond à un bit de $code.
// Traite les variables simples et les tableaux.
	global $debug;
	if ($debug & $code) {
		echo "<pre>[[[".$msg ;
		if ($var) {
			echo " : ";
			if (is_array ($var) || is_numeric ($var) || empty ($mix) ) {			
				print_r ($var); 
			} else  {
				for ($i=0; $i<strlen($var); $i+=1) {
					echo ".".substr ($var, $i, 1);
				}
			}			
		} 
		echo "]]]</pre>";
	}
	return;
}

?>
